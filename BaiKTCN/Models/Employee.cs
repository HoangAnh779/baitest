﻿using System;
using System.Collections.Generic;

namespace BaiKTCN.Models;

public partial class Employee
{
    public int EmployeeId { get; set; }

    public string? Fisrtname { get; set; }

    public string? Lastname { get; set; }

    public string? ContactAndAddress { get; set; }

    public virtual ICollection<Transactional> Transactionals { get; set; } = new List<Transactional>();
}
