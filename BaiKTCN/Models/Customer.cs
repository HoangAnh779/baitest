﻿using System;
using System.Collections.Generic;

namespace BaiKTCN.Models;

public partial class Customer
{
    public int CustomerId { get; set; }

    public string? Firstname { get; set; }

    public string? LastName { get; set; }

    public string? ContactAndAddress { get; set; }

    public virtual ICollection<Account> Accounts { get; set; } = new List<Account>();

    public virtual ICollection<Transactional> Transactionals { get; set; } = new List<Transactional>();
}
